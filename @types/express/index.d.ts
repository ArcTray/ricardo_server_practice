export {};

declare global {
	namespace Express {
		interface Request {
			userData: string | object;
		}
	}
}
