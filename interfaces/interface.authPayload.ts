interface AuthenticationPayload {
	email: string;
	userId: string;
	iat: number;
	exp: number;
}

export = AuthenticationPayload;
