import { Document } from 'mongoose';
import IProduct from './interface.product';

interface IProductOrder extends Document {
	productId: IProduct['_id'];
	quantity: number;
}

export = IProductOrder;
