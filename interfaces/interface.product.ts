import { Document, Types } from 'mongoose';

interface IProduct extends Document {
	_id: Types.ObjectId;
	name: string;
	price: number;
}

export = IProduct;
