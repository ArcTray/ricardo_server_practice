import { Document, Types } from 'mongoose';
import IProductOrder from './interface.productOrder';

interface IOrder extends Document {
	_id: Types.ObjectId;
	productsId: IProductOrder[];
}

export = IOrder;
