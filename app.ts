/**
 * Required External Modules
 */

//Global packages
import * as dotenv from 'dotenv';
import express, { NextFunction } from 'express';
import cors from 'cors';
import helmet from 'helmet';
import morgan from 'morgan';

//Utility methods
import dbMethods from './utils/database.connection';

// Routes import
import productRoutes from './routes/routes.products';
import orderRoutes from './routes/routes.orders';
import userRoutes from './routes/routes.user';

// Classes and interfaces import
import HttpException from './exceptions/HttpException';
import { Server } from 'http';

dotenv.config();

/**
 * App Variables
 */

if (!process.env.PORT) {
	process.exit(1);
}
const PORT: number = parseInt(process.env.PORT as string, 10);

const app: express.Application = express();

/**
 *  App Configuration
 */

//Database configuration
dbMethods.dbConnection();
// dbMethods.setDbErrorEvent();
dbMethods.setDbConnectionEvent();

//General middlewares
app.use(helmet());
app.use(cors());
app.use(morgan('dev'));
app.use('/uploads', express.static('uploads'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

//Routes to handle request from the Router
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);
app.use('/user', userRoutes);

//404 error handler
app.use('*', (req: express.Request, res: express.Response, next: NextFunction) => {
	res.status(404).json({ statusCode: 404, message: 'No available endpoint' });
});

//Error handling middleware
app.use((err: HttpException, req: express.Request, res: express.Response, next: NextFunction) => {
	//Wrongly formated JSON request
	if (err instanceof SyntaxError) {
		return res.status(400).json({
			statusCode: 400,
			err: 'Invalid JSON',
		});
	}

	//General error handling
	return res.status(err.status || 500).json({
		statusCode: err.status || 500,
		err: err.message,
	});
});

/**
 * Server Activation
 */

const server: Server = app.listen(PORT, () => {
	console.log(`Listening on port ${PORT}`);
});
