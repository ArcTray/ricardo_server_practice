import express, { NextFunction } from 'express';
import IAuthenticationPayload from '../interfaces/interface.authPayload';

import moment from 'moment';
import jwt from 'jsonwebtoken';

class AuthStore {
	/**
	 *	Create a JWT with set payload
	 *
	 * @static
	 * @param {string} email - User email to insert into JWT
	 * @param {string} userId - User ID to insert into JWT
	 * @returns {string} JWT to send in response
	 * @memberof UserStore
	 */
	static createToken(email: string, userId: string) {
		try {
			const payload: IAuthenticationPayload = {
				email: email,
				userId: userId,
				iat: moment().unix(),
				exp: moment().add(1, 'hour').unix(),
			};

			const JWT: string = jwt.sign(payload, process.env.JWT_KEY as string);

			return JWT;
		} catch (error) {
			return new Error('Error in hashing of password');
		}
	}

	/**
	 * Middleware to check authentication
	 *
	 * @static
	 * @param {express.Request} req - Express request object
	 * @param {express.Response} res - Express response object
	 * @param {NextFunction} next - Express next fucntion
	 * @returns
	 * @memberof AuthStore
	 */
	static authenticate(req: express.Request, res: express.Response, next: NextFunction) {
		try {
			//Authorization token should be passed as a header
			const decoded = AuthStore.consumeToken(req);

			if (decoded instanceof Error) {
				throw decoded;
			}
			//Set the information on the request object
			req.userData = decoded;

			//Continue into next middleware
			next();
		} catch (error) {
			return res.status(401).json({ statusCode: 401, message: error.message });
		}
	}

	/**
	 *  Verify JWT token for authorization
	 *
	 * @static
	 * @param {express.Request} req - Request object from express
	 * @returns {IAuthenticationPayload} - JWT Payload
	 * @memberof AuthStore
	 */
	static consumeToken(req: express.Request) {
		try {
			//Make sure the request has an authorization header
			if (!req.headers.authorization) {
				return new Error('Please make sure your request has an authorization header');
			}
			//Split the authorization header, removing the type and the token

			const token = req.headers.authorization.split(' ')[1];
			const type = req.headers.authorization.split(' ')[0];
			let payload: IAuthenticationPayload;
			//Check for type of authentication
			switch (type) {
				case 'Bearer':
					payload = jwt.verify(token, (process.env.JWT_KEY as unknown) as string) as IAuthenticationPayload;
					break;
				default:
					return new Error('Invalid token type.  Must be type Bearer');
			}
			if (!payload || !payload.userId) {
				return new Error('Authorization Denied');
			}

			return payload;
		} catch (error) {
			return new Error(error.message);
		}
	}
}

export = AuthStore;
