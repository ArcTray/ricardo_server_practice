import Order from '../models/models.order';
import { Types } from 'mongoose';
import IOrder from '../interfaces/interface.order';
import IProductorder from '../interfaces/interface.productOrder';

class OrderStore {
	/**
	 *  Retrieve all orders from the DB
	 *
	 * @static
	 * @param {string} id - ID of the order to find
	 * @returns {Promise} Promise that resolves in Product or error
	 * @memberof OrderStore
	 */
	static async retrieveAllOrders() {
		try {
			let foundOrders: IOrder[] | null = await Order.find()
				.select('_id ProdsInOrder')
				.populate('ProdsInOrder.productId', '_id name price')
				.exec();

			if (foundOrders) {
				return foundOrders;
			} else {
				throw Error;
			}
		} catch (error) {
			return new Error("Couldn't find order with that ID in the DB");
		}
	}

	/**
	 *  Add single order to DB
	 *
	 * @static
	 * @param {Array<IProductorder>} product - Ids and quantity of the products to order
	 * @returns
	 * @memberof OrderStore
	 */
	static async createSingleOrder(ProdsInOrder: Array<IProductorder>) {
		try {
			const newOrder = new Order({
				_id: new Types.ObjectId(),
				ProdsInOrder: ProdsInOrder,
			});

			let createdOrder = await newOrder.save();

			return createdOrder;
		} catch (error) {
			return new Error("Couldn't post order to the DB");
		}
	}

	/**
	 *  Retrieve single order from the DB
	 *
	 * @static
	 * @param {string} id - ID of the order to find
	 * @returns {Promise} Promise that resolves in Product or error
	 * @memberof OrderStore
	 */
	static async retrieveSingleOrder(id: string) {
		try {
			let foundOrder: IOrder | null = await Order.findById(id)
				.select('_id ProdsInOrder')
				.populate('ProdsInOrder.productId', '_id name price')
				.exec();

			if (foundOrder) {
				return foundOrder;
			} else {
				throw Error;
			}
		} catch (error) {
			return new Error("Couldn't find order with that ID in the DB");
		}
	}

	/**
	 *  Update single order from the DB
	 *
	 * @static
	 * @param {string} id - ID of the order to find and update
	 * @param {Object} updateOps - Object with the fields and values to update
	 * @returns {Promise} Promise that resolves in Order or error
	 * @memberof ProductStore
	 */
	static async updateSingleProduct(id: string, updateOps: Object) {
		try {
			let foundOrder: IOrder | null = await Order.findByIdAndUpdate(id, { $set: updateOps }).exec();

			if (foundOrder) {
				return foundOrder;
			} else {
				throw Error;
			}
		} catch (error) {
			return new Error("Couldn't find order with that ID in the DB");
		}
	}
	/**
	 *  Delete single order from the DB
	 *
	 * @static
	 * @param {string} id - ID of the order to find
	 * @returns {Promise} Promise that resolves in Order or error
	 * @memberof ProductStore
	 */
	static async deleteSingleOrder(id: string) {
		try {
			let deletedProduct: IOrder | null = await Order.findByIdAndDelete(id).exec();

			if (deletedProduct) {
				return deletedProduct;
			} else {
				throw Error;
			}
		} catch (error) {
			return new Error("Couldn't delete order with that ID in the DB");
		}
	}
}

export = OrderStore;
