import Product from '../models/models.product';
import { Types } from 'mongoose';
import IProduct from '../interfaces/interface.product';

class ProductStore {
	/**
	 * Retrieve all products from the DB
	 *
	 * @static
	 * @returns {Promise} Promise that resolves in array of Products or error
	 * @memberof ProductStore
	 */
	static async retrieveAllProducts() {
		try {
			let foundProducts: IProduct[] | null = await Product.find().select('_id name price productImage').exec();

			if (foundProducts) {
				return foundProducts;
			} else {
				throw Error;
			}
		} catch (error) {
			return new Error("Couldn't find elements in the DB");
		}
	}

	/**
	 *  Add single product to DB
	 *
	 * @static
	 * @param {string} name - Name of the product
	 * @param {number} price - Price of the product
	 * @param {string} productImage - Path to the product image
	 * @returns {Promise} Promise that resolves in created product or error
	 * @memberof ProductStore
	 */
	static async createSingleProduct(name: string, price: number, productImage: string) {
		try {
			const newProduct = new Product({
				_id: new Types.ObjectId(),
				name: name,
				price: price,
				productImage: productImage,
			});

			let createdProduct = await newProduct.save();

			return createdProduct;
		} catch (error) {
			return new Error("Couldn't post product to the DB");
		}
	}

	/**
	 *  Retrieve single product from the DB
	 *
	 * @static
	 * @param {string} id - ID of the product to find
	 * @returns {Promise} Promise that resolves in Product or error
	 * @memberof ProductStore
	 */
	static async retrieveSingleProduct(id: string) {
		try {
			let foundProduct: IProduct | null = await Product.findById(id).select('_id name price productImage').exec();

			if (foundProduct) {
				return foundProduct;
			} else {
				throw Error;
			}
		} catch (error) {
			return new Error("Couldn't find product with that ID in the DB");
		}
	}

	/**
	 *  Update single product from the DB
	 *
	 * @static
	 * @param {string} id - ID of the product to find and update
	 * @param {Object} updateOps - Object with the fields and values to update
	 * @returns {Promise} Promise that resolves in Product or error
	 * @memberof ProductStore
	 */
	static async updateSingleProduct(id: string, updateOps: Object) {
		try {
			let foundProduct: IProduct | null = await Product.findByIdAndUpdate(id, { $set: updateOps }).exec();

			if (foundProduct) {
				return foundProduct;
			} else {
				throw Error;
			}
		} catch (error) {
			return new Error("Couldn't find product with that ID in the DB");
		}
	}
	/**
	 *  Delete single product from the DB
	 *
	 * @static
	 * @param {string} id - ID of the product to find
	 * @returns {Promise} Promise that resolves in Product or error
	 * @memberof ProductStore
	 */
	static async deleteSingleProduct(id: string) {
		try {
			let deletedProduct: IProduct | null = await Product.findByIdAndDelete(id).exec();

			if (deletedProduct) {
				return deletedProduct;
			} else {
				throw Error;
			}
		} catch (error) {
			return new Error("Couldn't delete product with that ID in the DB");
		}
	}
}

export = ProductStore;
