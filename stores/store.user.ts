import User from '../models/models.user';
import { Types } from 'mongoose';
import IUser from '../interfaces/interface.user';
import AuthStore from '../stores/store.auth';

import bcrypt from 'bcrypt';

class UserStore {
	/**
	 * Create user in DB, checking existence and hashing password
	 *
	 * @static
	 * @param {string} email - User email to store
	 * @param {string} password - User password to store
	 * @returns {IUser} Created user
	 * @memberof UserStore
	 */
	static async createUser(email: string, password: string) {
		try {
			const foundUser = await User.find({ email: email }).exec();

			if (foundUser.length >= 1) {
				throw new Error('User already exists in the DB');
			}

			const hashedPassword: string | Error = await UserStore.hashPassword(10, password);

			if (hashedPassword instanceof Error) {
				throw hashedPassword;
			}

			const newUser: IUser = new User({
				_id: new Types.ObjectId(),
				email: email,
				password: hashedPassword,
			});

			const createdUser = await newUser.save();

			if (createdUser) {
				return createdUser;
			} else {
				throw Error("Couldn't create user in the DB");
			}
		} catch (error) {
			return new Error(error.message);
		}
	}

	/**
	 * Delete user from DB
	 *
	 * @static
	 * @param {string} email - User email to find
	 * @param {string} password - User password to compare
	 * @returns {IUser} Deleted user
	 * @memberof UserStore
	 */
	static async deleteUser(email: string, password: string) {
		try {
			const foundUser = await User.findOne({ email: email }).exec();

			if (!foundUser) {
				throw new Error("User doesn't exist in the DB");
			}
			const matched = await bcrypt.compare(password, foundUser.password);

			if (!matched) {
				throw new Error('Password incorrect');
			}

			const deletedUser = await User.findByIdAndDelete(foundUser._id).exec();

			if (deletedUser) {
				return deletedUser;
			} else {
				throw Error("Couldn't delete user from the DB");
			}
		} catch (error) {
			return new Error(error.message);
		}
	}

	/**
	 * Login user
	 *
	 * @static
	 * @param {string} email - User email to login
	 * @param {string} password - User password to login
	 * @returns {IUser} JSON Token
	 * @memberof UserStore
	 */
	static async loginUser(email: string, password: string) {
		try {
			const foundUser = await User.findOne({ email: email }).exec();

			if (!foundUser) {
				throw new Error('Authentication failed');
			}
			const matched = await bcrypt.compare(password, foundUser.password);

			if (!matched) {
				throw new Error('Authentication failed');
			}

			const JWT = AuthStore.createToken(foundUser.email, (foundUser._id as unknown) as string);

			if (JWT instanceof Error) {
				throw JWT;
			}

			return JWT;
		} catch (error) {
			return new Error(error.message);
		}
	}

	/**
	 * Hash password using bcrypt implementation
	 *
	 * @static
	 * @param {number} saltRounds - Number of salt rounds
	 * @param {string} plainPassword - Plaint text password
	 * @returns {Promise} Promise that resolves in hashed password or error
	 * @memberof UserStore
	 */
	static async hashPassword(saltRounds: number, plainPassword: string) {
		try {
			const salt = await bcrypt.genSalt(saltRounds);

			const hash = await bcrypt.hash(plainPassword, salt);

			return hash;
		} catch (error) {
			return new Error('Error in hashing of password');
		}
	}
}

export = UserStore;
