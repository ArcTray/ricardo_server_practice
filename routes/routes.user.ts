import UserController from '../controllers/controller.user';
import express, { Router } from 'express';

const router: Router = express.Router();

router.post('/signup', UserController.postCreateUser);

router.post('/login', UserController.postLoginUser);

router.delete('/', UserController.deleteUser);

export = router;
