import express, { Router } from 'express';
import productController from '../controllers/controller.products';
import MulterImplementation from '../utils/filehandler.multer';
import AuthStore from '../stores/store.auth';

const upload = MulterImplementation.implement();
const router: Router = express.Router();

//Handle incoming GET request to /products
router
	.get('/', productController.getAllProducts)
	.post('/', AuthStore.authenticate, upload.single('productImage'), productController.postSingleProduct);

router
	.get('/:productId', productController.getSingleProduct)
	.patch('/:productId', AuthStore.authenticate, productController.patchSingleProduct)
	.delete('/:productId', AuthStore.authenticate, productController.deleteSingleProduct);

export = router;
