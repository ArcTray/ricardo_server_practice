import express, { Router } from 'express';
import ordersController from '../controllers/controller.orders';
import AuthStore from '../stores/store.auth';

const router: Router = express.Router();

router
	.get('/', AuthStore.authenticate, ordersController.getAllOrders)
	.post('/', AuthStore.authenticate, ordersController.postSingleOrder);

router.get('/:orderId', AuthStore.authenticate, ordersController.getSingleOrder);

export = router;
