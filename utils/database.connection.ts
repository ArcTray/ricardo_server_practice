import mongoose from 'mongoose';

import HttpException from '../exceptions/HttpException';

class dbMethods {
	static async dbConnection(): Promise<void> {
		try {
			let uri = `mongodb+srv://${process.env.MONGODB_USER}:${process.env.MONGODB_PW}@databaseapi-qcx48.mongodb.net/test?retryWrites=true&w=majority`;

			mongoose.connect(uri, {
				useNewUrlParser: true,
				useUnifiedTopology: true,
				useCreateIndex: true,
			});
		} catch (error) {
			throw new HttpException(500, error);
		}
	}
	static setDbConnectionEvent(): void {
		mongoose.connection.once('open', () => {
			console.log('Connection succesful');
		});
	}

	// static setDbErrorEvent(): void {
	// 	mongoose.connection.on('error', (err) => {
	// 		throw new HttpException(500, err);
	// 	});
	// }
}

export = dbMethods;
