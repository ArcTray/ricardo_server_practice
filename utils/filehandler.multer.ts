import multer from 'multer';
import HttpException from '../exceptions/HttpException';

class MulterImplementation {
	/**
	 *  Create a multer instance with parameters set in class
	 *
	 * @static
	 * @returns
	 * @memberof MulterImplementation
	 */
	static implement() {
		//Set implementation details

		const storageConfig = multer.diskStorage({
			destination: (req, file, cb) => {
				cb(null, './uploads');
			},
			filename: (req: Express.Request, file: Express.Multer.File, cb: Function) => {
				cb(null, Date.now() + file.originalname);
			},
		});

		const limitsConfig = { fileSize: 1024 * 1024 * 5 };

		const fileFilterCallback = (req: Express.Request, file: Express.Multer.File, cb: Function) => {
			if (file.mimetype === ('image/jpeg' || 'image/png')) {
				cb(null, true);
			} else {
				cb(new HttpException(415, 'Unvalid image format'), false);
			}
		};

		const upload = multer({
			storage: storageConfig,
			limits: limitsConfig,
			fileFilter: fileFilterCallback,
		});

		return upload;
	}
}

export = MulterImplementation;
