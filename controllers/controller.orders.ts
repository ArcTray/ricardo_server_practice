import express, { NextFunction } from 'express';
import OrderStore from '../stores/store.orders';

import HttpException from '../exceptions/HttpException';
import IOrder from '../interfaces/interface.order';

class OrderController {
	static async getAllOrders(req: express.Request, res: express.Response, next: NextFunction) {
		try {
			let foundOrders = await OrderStore.retrieveAllOrders();

			const foundOrdersLength = foundOrders as Array<IOrder>;

			if (foundOrders instanceof Error) {
				throw foundOrders;
			}

			res.status(200).json({ statusCode: 200, amount: foundOrdersLength.length, foundOrders: foundOrders });
		} catch (error) {
			next(new HttpException(404, error.message));
		}
	}

	static async getSingleOrder(req: express.Request, res: express.Response, next: NextFunction) {
		try {
			let id: string = req.params.orderId;
			let foundOrder = await OrderStore.retrieveSingleOrder(id);

			if (foundOrder instanceof Error) {
				throw foundOrder;
			}

			res.status(200).json({ statusCode: 200, foundOrder: foundOrder });
		} catch (error) {
			next(new HttpException(404, error.message));
		}
	}

	static async postSingleOrder(req: express.Request, res: express.Response, next: NextFunction) {
		try {
			let createdOrder = await OrderStore.createSingleOrder(req.body.ProdsInOrder);

			if (createdOrder instanceof Error) {
				throw createdOrder;
			}

			res.status(201).json({ statusCode: 201, message: 'Order was created' });
		} catch (error) {
			next(new HttpException(400, error.message));
		}
	}

	static async patchSingleOrder(req: express.Request, res: express.Response, next: NextFunction) {
		try {
			const id: string = req.params.orderId;

			const updateOps = {};

			//Get the fields to patch on the database
			for (const key in req.body) {
				// @ts-ignore
				updateOps[key] = req.body[key];
			}

			const updatedOrder = await OrderStore.updateSingleProduct(id, updateOps);

			if (updatedOrder instanceof Error) {
				throw updatedOrder;
			}

			res.status(200).json({ statusCode: 200, message: 'Updated resource' });
		} catch (error) {
			next(new HttpException(404, error.message));
		}
	}

	static async deleteSingleProduct(req: express.Request, res: express.Response, next: NextFunction) {
		try {
			const id: string = req.params.orderId;
			const deletedProduct = await OrderStore.deleteSingleOrder(id);

			if (deletedProduct instanceof Error) {
				throw deletedProduct;
			}

			res.status(200).json({ statusCode: 200, message: 'Deleted resource' });
		} catch (error) {
			next(new HttpException(404, error.message));
		}
	}
}

export = OrderController;
