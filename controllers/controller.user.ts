import express, { NextFunction } from 'express';
import UserStore from '../stores/store.user';

import HttpException from '../exceptions/HttpException';

class UserController {
	static async postCreateUser(req: express.Request, res: express.Response, next: NextFunction) {
		try {
			const user = await UserStore.createUser(req.body.email, req.body.password);

			if (user instanceof Error) {
				throw user;
			}

			res.status(201).json({ statusCode: 201, message: 'User was created' });
		} catch (error) {
			next(new HttpException(401, error.message));
		}
	}

	static async postLoginUser(req: express.Request, res: express.Response, next: NextFunction) {
		try {
			const token = await UserStore.loginUser(req.body.email, req.body.password);

			if (token instanceof Error) {
				throw token;
			}

			res.status(200).json({ statusCode: 200, message: 'Authentication sucessful', token: token });
		} catch (error) {
			next(new HttpException(401, error.message));
		}
	}

	static async deleteUser(req: express.Request, res: express.Response, next: NextFunction) {
		try {
			const user = await UserStore.deleteUser(req.body.email, req.body.password);

			if (user instanceof Error) {
				throw user;
			}

			res.status(200).json({ statusCode: 200, message: 'User was deleted' });
		} catch (error) {
			next(new HttpException(401, error.message));
		}
	}
}

export = UserController;
