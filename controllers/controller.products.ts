import express, { NextFunction } from 'express';
import ProductStore from '../stores/store.products';

import HttpException from '../exceptions/HttpException';
import IProduct from '../interfaces/interface.product';

class ProductController {
	static async getAllProducts(req: express.Request, res: express.Response, next: NextFunction) {
		try {
			const foundProducts = await ProductStore.retrieveAllProducts();

			if (foundProducts instanceof Error) {
				throw foundProducts;
			}

			const foundProductsLength = foundProducts as Array<IProduct>;

			res.status(200).json({
				statusCode: 200,
				amount: foundProductsLength.length,
				resource: foundProducts,
			});
		} catch (error) {
			next(new HttpException(404, error.message));
		}
	}

	static async postSingleProduct(req: express.Request, res: express.Response, next: NextFunction) {
		try {
			const createdProduct = await ProductStore.createSingleProduct(req.body.name, req.body.price, req.file.path);

			if (createdProduct instanceof Error) {
				throw createdProduct;
			}

			res.status(201).json({ statusCode: 201, message: 'Created resource' });
		} catch (error) {
			next(new HttpException(400, error.message));
		}
	}

	static async getSingleProduct(req: express.Request, res: express.Response, next: NextFunction) {
		try {
			const id: string = req.params.productId;
			const foundProduct = await ProductStore.retrieveSingleProduct(id);

			if (foundProduct instanceof Error) {
				throw foundProduct;
			}

			res.status(200).json({ statusCode: 200, resource: foundProduct });
		} catch (error) {
			next(new HttpException(404, error.message));
		}
	}

	static async patchSingleProduct(req: express.Request, res: express.Response, next: NextFunction) {
		try {
			const id: string = req.params.productId;

			const updateOps = {};

			//Get the fields to patch on the database
			for (const key in req.body) {
				// @ts-ignore
				updateOps[key] = req.body[key];
			}

			const updatedProduct = await ProductStore.updateSingleProduct(id, updateOps);

			if (updatedProduct instanceof Error) {
				throw updatedProduct;
			}

			res.status(200).json({ statusCode: 200, message: 'Updated resource' });
		} catch (error) {
			next(new HttpException(404, error.message));
		}
	}

	static async deleteSingleProduct(req: express.Request, res: express.Response, next: NextFunction) {
		try {
			const id: string = req.params.productId;
			const deletedProduct = await ProductStore.deleteSingleProduct(id);

			if (deletedProduct instanceof Error) {
				throw deletedProduct;
			}

			res.status(200).json({ statusCode: 200, message: 'Deleted resource' });
		} catch (error) {
			next(new HttpException(404, error.message));
		}
	}
}

export = ProductController;
