import mongoose, { Schema } from 'mongoose';
import IOrder from '../interfaces/interface.order';

const ProdsInOrderSubSchema: Schema = new Schema({
	productId: { type: Schema.Types.ObjectId, required: true, ref: 'Product' },
	quantity: { type: Number, required: true, default: 1 },
});

const OrderSchema: Schema = new Schema({
	_id: Schema.Types.ObjectId,
	ProdsInOrder: { type: [ProdsInOrderSubSchema], default: undefined, required: true },
});

export default mongoose.model<IOrder>('Order', OrderSchema);
