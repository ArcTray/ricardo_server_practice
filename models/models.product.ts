import mongoose, { Schema } from 'mongoose';
import IProduct from '../interfaces/interface.product';

const ProductSchema: Schema = new Schema({
	_id: Schema.Types.ObjectId,
	name: { type: String, required: true },
	price: { type: Number, required: true },
	productImage: { type: String, required: true },
});

export default mongoose.model<IProduct>('Product', ProductSchema);
