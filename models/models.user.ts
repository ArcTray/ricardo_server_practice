import mongoose, { Schema } from 'mongoose';
import IUser from '../interfaces/interface.user';

const UserSchema: Schema = new Schema({
	_id: Schema.Types.ObjectId,
	email: {
		type: String,
		required: true,
		unique: true,
		match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
	},
	password: { type: String, required: true },
});

export default mongoose.model<IUser>('User', UserSchema);
